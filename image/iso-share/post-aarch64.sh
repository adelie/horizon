#!/bin/sh

mkdir -p cdroot/boot

cat >early.cfg <<'EARLYCFG'
search.fs_label "Adelie-aarch64" root
set prefix=($root)/boot
EARLYCFG

cat >cdroot/boot/grub.cfg <<'GRUBCFG'
menuentry "Adelie Linux Live (64-bit Arm)" --class linux --id adelie-live-cd-aarch64 {
        insmod iso9660
        insmod linux
        search --label "Adelie-aarch64" --no-floppy --set
        linux ($root)/kernel-aarch64 root=live:LABEL=Adelie-aarch64 rd.live.dir=/ rd.live.squashimg=aarch64.squashfs softlevel=graphical
        initrd ($root)/initrd-aarch64
}

menuentry "Adelie Linux Live (64-bit Arm) (text-only)" --class linux --id adelie-live-cd-aarch64-text {
        insmod iso9660
        insmod linux
        search --label "Adelie-aarch64" --no-floppy --set
        linux ($root)/kernel-aarch64 root=live:LABEL=Adelie-aarch64 rd.live.dir=/ rd.live.squashimg=aarch64.squashfs
        initrd ($root)/initrd-aarch64
}

menuentry "Reboot and Try Again" --class reboot --id reboot {
        reboot
}

GRUB_DEFAULT=adelie-live-cd
GRUB_TIMEOUT=10
GRUB_DISTRIBUTOR="Adelie"
GRUBCFG

if ! type grub-mkimage>/dev/null; then
	printf "GRUB image cannot be created.\n"
	exit 1
else
	printf '\033[01;32m * \033[37mInstalling GRUB...\033[00;39m\n'
	grub-mkimage -d target/usr/lib/grub/arm64-efi -c early.cfg -p boot -o efi64.exe -O arm64-efi boot btrfs datetime disk ext2 gfxmenu help iso9660 jfs ls luks lvm memdisk nilfs2 normal part_gpt part_msdos png scsi search xfs linux reboot gfxterm gfxterm_background gfxterm_menu all_video
fi

rm early.cfg

mkdir -p cdroot/System/Library/CoreServices
touch cdroot/System/Library/CoreServices/mach_kernel
cat >cdroot/System/Library/CoreServices/SystemVersion.plist <<PLIST
<plist version="1.0">
<dict>
	<key>ProductBuildVersion</key>
	<string>100</string>
	<key>ProductName</key>
	<string>Adélie Linux</string>
	<key>ProductVersion</key>
	<string>1.0</string>
</dict>
PLIST
cp efi64.exe cdroot/System/Library/CoreServices/boot.efi

if ! type mkfs.fat>/dev/null || ! type mtools>/dev/null; then
	printf "EFI image creation tools cannot be located.\n"
	exit 1
fi

cat >mtoolsrc <<-MTOOLSRC
drive A: file="efi64.img"
MTOOLSRC
export MTOOLSRC="$PWD/mtoolsrc"
dd if=/dev/zero of=efi64.img bs=1024 count=1440
mkfs.fat efi64.img
mmd A:/EFI
mmd A:/EFI/BOOT
mcopy efi64.exe A:/EFI/BOOT/BOOTAA64.EFI
rm efi64.exe mtoolsrc
