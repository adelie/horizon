===============================
 Changelog for Project Horizon
===============================
:Author:
  * **A. Wilcox**, documentation writer
  * **Contributors**, code
:Copyright:
  © 2019-2024 Adélie Linux and contributors.



0.9.9.1 (2024-12-14)
====================

HorizonScript Library
---------------------

* Firmware installs no longer add the APK Fission repository and keys to the
  list by default.  The user must now manually add APK Fission if they wish
  to install packages from APK Fission.  The linux-firmware package is now
  provided in the user/ repository.



0.9.9 (2024-11-25)
==================

Boot to Horizon
---------------

* Experimental ARMv7 support is now available.


Image Creation
--------------

* PowerPC disc images now create a /ppc/chrp directory, allowing boot on
  PowerVM LPARs.

* EFI tools are no longer optional when creating media on an architecture
  that supports EFI boot.  Images for x86-based platforms still support
  legacy BIOS boot, but EFI boot support will always be present.

* Various cleanups and fixes have been applied to ensure multiple image
  creations on the same node work properly.

* The tar backend now logs more status messages.

* The tar backend uses UTF-8 for filenames, fixing issues with some
  certificates from the ca-certificates package not being found in the
  target when the archive was extracted.


Qt UI
-----

* The Qt UI now counts APM partition map partitions when creating APM
  disklabels, fixing a long-standing bug when installing to an empty disk
  on PowerPC Macintosh systems.

* The SDDM service is enabled on boot when installed from Custom Packages.

* Manual network configuration now supports targets with a single network
  interface.

* Selecting "XFCE" as the desired desktop environment no longer installs
  all KDE packages.

* The manual partitioner can now be started on the install-only media.

* Keyboard layouts now show their "friendly" names in addition to their
  two-character ISO codes, allowing for easier identification.

* Usernames are now validated before allowing the user to choose Next,
  ensuring only valid usernames can be specified.



0.9.8.1 (2023-12-10)
====================

Qt UI
-----

* The Qt Runner now spawns the HorizonScript Executor with full root
  permissions, preventing an issue with file permissions in the target.



0.9.8 (2023-12-07)
==================

Boot to Horizon
---------------

* AArch64 support is now available.

* The minimise button has been removed from the title bars, since there is no
  UX to restore minimised windows.


Build system
------------

* CMake is now given the CWD to use for running tests, ensuring that project
  specific ``.valgrindrc`` settings are used.

* Support for ``std::filesystem`` has been removed.  It doesn't work on many
  platforms and is still not widely supported.  ``Boost.Filesystem`` is now
  used in both the Installation Environment and Runtime Environment.


Disk
----

* The ``diskid`` key now additionally probes the ``ID_MODEL_ENC`` udev property
  of the block device, to allow for matching on human-readable model names.

* The ``filesystem`` key now uses full paths for ``mkfs`` calls, which allows
  the Horizon Executor to run with a limited ``PATH`` environment variable.


HorizonScript Library
---------------------

* The ``inherit`` key can now be used more than once, to allow for multiple
  inheritance and mixins.  This is leveraged heavily in the rewrite of our
  Live Image configurations.

* The library now supports building with more recent GCC versions and conforms
  to the C++2x standard.


Image Creation
--------------

* The ``libarchive`` backend (``tar``) now builds correctly and uses
  ``Boost.Filesystem``.

* The CD backend (``iso``) supports creating media for AArch64 and ARMv7
  systems.  Both flavours of ARM presently only support EFI booting.

* All backends handle error conditions more reliably and probe for needed
  tools before beginning.

* CD backend generated GRUB configuration files are now readable on 64-bit
  PowerPC systems utilising the Petitboot boot loader.

* All images now support both graphical and text mode options in the GRUB
  menu, allowing to skip loading X11 on systems where it does not work.

* The ``libarchive`` backend (``tar``) now enables XZ threading support for
  significantly faster compression on systems with multiple processors.

* You may now specify a custom path to use for static Qemu binaries when
  generating cross-architecture media, using the ``-e`` or ``--emul-dir``
  option.  This option defaults to ``/usr/bin`` if not specified.


Metadata
--------

* The ``svcenable`` key now supports non-system runlevels.  All runlevels that
  are not recognised as system runlevels will be "stacked" with the default
  runlevel.  You can utilise these runlevels by passing the ``softlevel=NAME``
  parameter to the kernel command line, where ``NAME`` is the name of the
  desired runlevel.

* The default values for ``signingkey`` use the distfiles mirror instead
  of copies of the key on local disk.  This allows you to use System
  Installation from non-Adélie Linux systems.

* EFI installation has been made more reliable.

* The ``rootshell`` key has been added, which allows for setting the target
  system root user's login shell.  The default shell is ``/bin/sh`` and may
  be overridden with any executable present on the target system after
  package installation has completed.  This key will raise an error if the
  path provided does not specify an executable file in the target system.

* The ``pkginstall`` key now allows the selection of negative constraints.  For
  instance, you may specify ``pkginstall !foo-doc`` to avoid the installation
  of a package named ``foo-doc``.  Note that installation may fail if the APK
  solver cannot satisfy the constraint specified.


Qt UI
-----

* The location of the default APK repository signing keys has been updated.

* The software selection page now allows the user to choose which desktop
  environment they wish to install.  In the Runtime Environment, Plasma is
  still the default.  In the Installation Environment, the system attempts to
  default to the currently running environment.

* LibreOffice has been removed from the Qt UI entirely, since it is no longer
  packaged.  See adelie/packages@d7e5a71711.

* DHCP failures will no longer cause the wizard to stop processing.

* Tools launched from the Introduction page are now spawned instead of being
  run modally.  This allows user interaction with System Installation while
  the tools are still running.

* Date/time settings are now easier to input.  Clock options are now hidden in
  the Runtime Environment, as the host's clock has no bearing on the target.

* In the Installation Environment, a helper utility is now provided to
  automatically manipulate the Xauthority file during the hostname change from
  ``adelie-live`` to the target's hostname.

* The system now provides an Advanced Options screen accessible from the Tools
  dropdown of the Introduction page.  This allows the user to specify various
  advanced options.  This is not typically necessary and for expert users only.

* The "Use available space" option in the disk manipulation portion of the
  wizard has been removed.  It is recommended to instead use an external
  partitioning system, then use those created partitions in System Installation
  via the "Use existing partitions" option.

* The installed locale in the target environment is now en_US instead of en_GB.

* The Qt script runner now uses the same high-DPI scaling setting used by the
  rest of System Installation.

* The Qt script runner will now display any errors encountered when the user
  starts Xterm from the "Open Terminal" button.



0.9.7 (2022-09-04)
==================

Build system
------------

* The GitLab CI test environment is more predictible.


HorizonScript Library
---------------------

* The library now builds with LLVM libc++.

* Network configuration reading has been refactored to be more portable.


Documentation
-------------

* Miscellaneous URL fixes, documentation updates


Qt UI
-----

* The Standard and Mobile software selections now install the ``docs``
  metapackage, ensuring documentation is available on the installed system.

* Graphical Login via SDDM has been added to the Custom install.

* The Qt UI has been ported to the latest version of ``skalibs``.

* Multiple corrections and enhancements to build against Qt 5.15.

* Password salt material is now handled correctly.


Tests
-----

* Ruby will now always be invoked in UTF-8 mode, to support running the tests
  on different locales.

* The simulator tests will now pass on Darwin / Macintosh systems.



0.9.6 (2020-11-07)
==================

Disk
----

* Correctly fix EFI vars directory issue.


Image Creation
--------------

* An issue regarding creating archives with symlinks has been fixed.


Metadata
--------

* Horizon now runs ``update-boot`` when bootloader installation is requested.


Network
-------

* PPPoE: An issue reading certain configuration keys has been fixed.


Tests
-----

* Many additional test cases have been added, bringing code coverage to nearly
  97%.



0.9.5 (2020-09-27)
==================

Disk
----

* Remount EFI vars directory correctly during UEFI bootloader installation.

* Force creation of ext4 file systems.


Image Creation
--------------

* Dracut is now called inside the chroot, making it easier to create foreign
  architecture media.


Qt UI
-----

* An issue with installing bootloaders has been fixed.

* The Wi-Fi network selection page has been redesigned to be easier to use, and
  now has a help file.

* It is now possible to format partitions from the manual mount selection page.

* A build issue with unstable versions of Qt has been fixed.  Note that non-LTS
  versions of Qt are still officially unsupported.  If you have issues using a
  self-built version of Horizon, please build against Qt 5.12 before reporting.

* It is now possible to build the Runtime Environment wizard for Macintosh.

* The manual mount code was rewritten to perform faster in complex scenarios.

* Dracut will now be installed to the target when the bootloader is selected.



0.9.4 (2020-08-30)
==================

Boot to Horizon
---------------

* Add ``gfxdetect`` oneshot, to reassure the user that System Installation
  is starting.


Disk
----

* Create Apple Partition Map disk labels correctly.


Documentation
-------------

* The JSON schema used by ``hscript-fromjson`` has been documented in
  Chapter 4 of the HorizonScript Reference Manual.


HorizonScript Library
---------------------

* Work around a bug in Alpine Linux that prevents ``/dev/stdin`` from being
  canonicalised.

* Add initial support for MIPS.  This does not include an automatic
  partitioner, which is required for Tier 1 support.


Image Creation
--------------

* CDs now use Dracut for initramfs instead of the custom init from 2016.
  This also means that live media can load modules instead of relying on
  only built-in kernel drivers.


Metadata
--------

* If a service has already been enabled via an external process, such as
  having an entry in /etc/runlevels from the packaging itself, the
  ``svcenable`` key will no longer cause script execution to fail.

* The ``bootloader`` key now takes two parameters, with the first one
  being a required device specification for where to install the system
  bootloader.  The second parameter is identical to the original specification
  of ``bootloader``, except that the ``false`` value is no longer valid.


Qt UI
-----

* All references to ConsoleKit have been replaced with elogind.


Tests
-----

* Several tests were added to ensure continued correctness of various metadata
  keys, including:

  * Non-alphanumeric ``hostname`` s should fail.

  * Correctness of the ``version`` key implementation.

  * Various usages, valid and invalid, of the ``bootloader`` key.



0.9.3 (2020-07-06)
==================

Boot to Horizon
---------------

* Set X11 root window colour to calming blue.

* Disable compositing in KWin, which can cause issues in Qemu.


Build system
------------

* Only search for the BCNM library when building the Qt UI for Installation.

* Add ``BUILD_ISO`` toggle for building the ISO backend for Image Creation.


HorizonScript Library
---------------------

* Ensure mount directory tree exists before attempting to create the actual
  directory.

* Recursively bind-mount ``/dev`` and ``/sys`` in the target.

* When using the ImageOnly flag, don't ensure mountpoint devices are available.

* Fix issue where installation on live media failed due to network state.


Image Creation
--------------

* Add 'keep' option for ISO backend to preserve target dir between runs.

* Ensure ``init`` binary is executable in ISO initramfs.

* Correctly read scripts from stdin.


JSON
----

* Add ``service`` node support, which corresponds to the ``svcenable`` key.


Metadata
--------

* Add new ``bootloader`` key, to control installation of bootloaders.

* Fixes for EFI installations.

* Add optional runlevel parameter to ``svcenable`` key.


Network
-------

* Allow hostnames to start with numeric characters.  This represents a
  *potentially breaking change* to systems that require RFC 952 compliance.

* Add new ``pppoe`` key for configuring PPPoE links.


Qt UI
-----

* Properly flush log file to disk.

* Add workaround for network issue.

* Install SDDM for graphical installation types.

* Use bootloader key when a bootloader is requested.

* Fix various minor issues in custom package selection screen.

* Use ``vfat`` instead of ``fat32`` for ESP filesystem type.

* Correctly enable udev when udev is selected.




0.9.2 (2020-06-06)
==================

Boot to Horizon
---------------

* Added resources to enable booting to the System Installation Wizard.


Metadata
--------

* Update ``keymap`` to use kbd instead of console-setup.


Qt UI
-----

* Ensure log is opened properly when built in Release modes.

* Fix up signing keys, installed packages, and enabled services.




0.9.1 (2020-06-03)
==================

Build
-----

* Don't build the printowner utility in the Runtime Environment.


HorizonScript Library
---------------------

* Add new ScriptLocation object to more accurately report warnings and errors.

* Target mounts are no longer hardcoded to be a subdirectory of ``/target``.

* APK ``--keys-dir`` argument is now relative.

* Files downloaded from the network are written properly before execution
  continues, ensuring partial writes do not corrupt installation state.
 
* Execution of user keys works in cross-architecture script execution contexts.


Image Creation
--------------

* Run ``prepare`` before script execution.

* A root mount is no longer required to generate an image.

* The target is now a subdirectory of the IR dir, instead of the IR dir itself.

* Backends can now accept options via ``-b``.

* An ISO backend has been added, allowing generation of Live CDs.


Metadata
--------

* Add the ``svcenable`` key, allowing configuration of default services.

* Add the ``version`` key, allowing the version of Adélie to be selected.

* ``hostname``: Create ``/etc`` in the target if it does not yet exist.  This
  is required to properly generate Alpine Linux images.

* ``timezone``: Remove the existing ``/etc/localtime`` in all cases.


Qt UI
-----

* Fix building for Runtime Environment.

* Properly support High DPI screens.

* Properly check length of root passphrase.

* Allow mountpoints to be specified in the Runtime Environment.

* Generate a valid installfile if no packages are selected in Custom mode.

* Use QString helper to avoid GCC warning about snprintf bounds.




0.9.0 (2020-05-23)
==================

Build
-----

* The BCNM check is now functional.

Disk
----

* HFS+ has been added as a supported file system.

* disklabel: MBR support has been fixed.

* partition: Add 'bios' and 'prep' flags.

* partition: Type codes are now handled properly.

* partition: Ensure the first partition isn't too close to the first sectors.

* fs: Undo files for e2fs are no longer created.

* mount: Each mount key no longer overwrites /etc/fstab in the target.

DiskMan
-------

* A new library for probing disk information, DiskMan, has been written.

Documentation
-------------

* All tools and libraries, except libhscript, have been documented with
  manual pages.

HorizonScript Library
---------------------

* Introspection support has been added to the Script class.

* Refactored Keys to be owned by a Script, so Keys can access the values of
  other keys.

* The target directory is now configurable.

* /dev, /proc, and /sys are now mounted in the target.

Image Creation
--------------

* A new system for creating images using the Horizon system has been written.

Metadata
--------

* arch: New key added, including specification and implementation.

* repository: Fixed defaults when 'firmware' is set to true.

* timezone: Fixed issue when target already contained /etc/localtime.

Network
-------

* Configure network interfaces inside the Install Environment when
  'network' is set to true.

* netconfigtype: New key added, including specification and implementation.

* Existing network configuration on the Installation Environment system is
  now properly handled.

Package
-------

* APK Tools are now invoked more efficiently.

* --keys-dir is now passed to APK Tools during base installation.

Project
-------

* A Code of Conduct has been added.

* A YANG model, describing a JSON schema for representing a HorizonScript,
  has been written.

Tests
-----

* A test has been added for IPv4 subnet -> CIDR conversion.

Tools
-----

* Use Boost's program_options instead of vendoring clipp.  Thanks to
  Calvin Buckley.

* Messages have been unified between the CLI tools, to ensure consistency.

* A new tool, hscript-fromjson, has been added to convert JSON representations
  of HorizonScripts to HorizonScript.

UI
--

* The Qt 5 UI has been added.  It is not yet fully implemented, but basic
  installations should work.

User
----

* userpassphrase: Ensure simulated runs don't actually set passwords.

* All user keys now run shadow commands in the target, instead of the
  Installation Environment system.

Util
----

* Factor subnet -> CIDR conversion to util function.




0.2.0 (2019-11-07)
==================

Disk
----

* lvm_pv, lvm_vg, and lvm_lv execution are now implemented.


Metadata
--------

* keymap execution is now implemented.

* language: An issue with execution of the language key has been fixed.

* signingkey: Firmware keys are now installed when firmware is true.


Network
-------

* hostname: dns_domain_lo is now properly set in target /etc/conf.d/net.

* nameserver execution is now implemented.

* netaddress: OpenRC services are now added for configured interfaces.


Owner
-----

* New utility 'hscript-printowner' added, which prints the owning UID of a
  given path.


User
----

* User account creation is now fully implemented.




0.1.0 (2019-11-02)
==================

Initial release.
