/*
 * user.hh - User account constant definitions
 * util, the utility library for
 * Project Horizon
 *
 * Copyright (c) 2024 Adélie Linux and contributors.  All rights reserved.
 * This code is licensed under the AGPL 3.0 license, as noted in the
 * LICENSE-code file in the root directory of this repository.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef HORIZON_USER_HH_
#define HORIZON_USER_HH_

#include <set>
#include <string>

const static std::set<std::string> system_names = {
        "root", "bin", "daemon", "adm", "lp", "sync", "shutdown", "halt", "mail",
        "news", "uucp", "operator", "man", "postmaster", "cron", "ftp", "sshd",
        "at", "squid", "xfs", "games", "postgres", "cyrus", "vpopmail", "utmp",
        "catchlog", "alias", "qmaild", "qmailp", "qmailq", "qmailr", "qmails",
        "qmaill", "ntp", "smmsp", "guest", "nobody"
};

const static std::set<std::string> system_groups = {
        "root", "bin", "daemon", "sys", "adm", "tty", "disk", "lp", "mem", "kmem",
        "wheel", "floppy", "mail", "news", "uucp", "man", "cron", "console",
        "audio", "cdrom", "dialout", "ftp", "sshd", "input", "at", "tape", "video",
        "netdev", "readproc", "squid", "xfs", "kvm", "games", "shadow", "postgres",
        "cdrw", "usb", "vpopmail", "users", "catchlog", "ntp", "nofiles", "qmail",
        "qmaill", "smmsp", "locate", "abuild", "utmp", "ping", "nogroup", "nobody"
};

#endif /* !HORIZON_USER_HH_ */
