/*
 * keymaps.hh - Keymap definitions
 * util, the utility library for
 * Project Horizon
 *
 * Copyright (c) 2019 Adélie Linux and contributors.  All rights reserved.
 * This code is licensed under the AGPL 3.0 license, as noted in the
 * LICENSE-code file in the root directory of this repository.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef HORIZON_KEYMAPS_HH_
#define HORIZON_KEYMAPS_HH_

#include <map>
#include <string>

const std::map<std::string, std::string> valid_keymaps = {
    {"us", "English (US)"}, {"af", "Afghani"}, {"ara", "Arabic"},
    {"al", "Albanian"}, {"am", "Armenian"}, {"at", "German (Austria)"},
    {"au", "English (Australian)"}, {"az", "Azerbaijani"}, {"by", "Belarusian"},
    {"be", "Belgian"}, {"bd", "Bangla"}, {"in", "Indian"}, {"ba", "Bosnian"},
    {"br", "Portuguese (Brazil)"}, {"bg", "Bulgarian"},
    {"dz", "Kabylian (azerty layout, no dead keys)"}, {"ma", "Arabic (Morocco)"},
    {"cm", "English (Cameroon)"}, {"mm", "Burmese"}, {"ca", "French (Canada)"},
    {"cd", "French (Democratic Republic of the Congo)"}, {"cn", "Chinese"},
    {"hr", "Croatian"}, {"cz", "Czech"}, {"dk", "Danish"}, {"nl", "Dutch"},
    {"bt", "Dzongkha"}, {"ee", "Estonian"}, {"ir", "Persian"}, {"iq", "Iraqi"},
    {"fo", "Faroese"}, {"fi", "Finnish"}, {"fr", "French"}, {"gh", "English (Ghana)"},
    {"gn", "French (Guinea)"}, {"ge", "Georgian"}, {"de", "German"}, {"gr", "Greek"},
    {"hu", "Hungarian"}, {"is", "Icelandic"}, {"il", "Hebrew"}, {"it", "Italian"},
    {"jp", "Japanese"}, {"kg", "Kyrgyz"}, {"kh", "Khmer (Cambodia)"}, {"kz", "Kazakh"},
    {"la", "Lao"}, {"latam", "Spanish (Latin American)"}, {"lt", "Lithuanian"},
    {"lv", "Latvian"}, {"mao", "Maori"}, {"me", "Montenegrin"}, {"mk", "Macedonian"},
    {"mt", "Maltese"}, {"mn", "Mongolian"}, {"no", "Norwegian"}, {"pl", "Polish"},
    {"pt", "Portuguese"}, {"ro", "Romanian"}, {"ru", "Russian"}, {"rs", "Serbian"},
    {"si", "Slovenian"}, {"sk", "Slovak"}, {"es", "Spanish"}, {"se", "Swedish"},
    {"ch", "German (Switzerland)"}, {"sy", "Arabic (Syria)"}, {"tj", "Tajik"},
    {"lk", "Sinhala (phonetic)"}, {"th", "Thai"}, {"tr", "Turkish"}, {"tw", "Taiwanese"},
    {"ua", "Ukrainian"}, {"gb", "English (UK)"}, {"uz", "Uzbek"}, {"vn", "Vietnamese"},
    {"kr", "Korean"}, {"nec_vndr/jp", "Japanese (PC-98)"}, {"ie", "Irish"},
    {"pk", "Urdu (Pakistan)"}, {"mv", "Dhivehi"}, {"za", "English (South Africa)"},
    {"epo", "Esperanto"}, {"np", "Nepali"}, {"ng", "English (Nigeria)"},
    {"et", "Amharic"}, {"sn", "Wolof"}, {"brai", "Braille"}, {"tm", "Turkmen"},
    {"ml", "Bambara"}, {"tz", "Swahili (Tanzania)"}, {"tg", "French (Togo)"},
    {"ke", "Swahili (Kenya)"}, {"bw", "Tswana"}, {"ph", "Filipino"}, {"md", "Moldavian"},
    {"id", "Indonesian (Arab Melayu, phonetic)"}, {"jv", "Indonesian (Javanese)"},
    {"my", "Malay (Jawi, Arabic Keyboard)"}
};

#endif  /* !HORIZON_KEYMAPS_HH_ */
