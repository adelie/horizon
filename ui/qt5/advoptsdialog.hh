/*
 * advoptsdialog.hh - Definition of the UI.AdvOpts page
 * horizon-qt5, the Qt 5 user interface for
 * Project Horizon
 *
 * Copyright (c) 2019 Adélie Linux and contributors.  All rights reserved.
 * This code is licensed under the AGPL 3.0 license, as noted in the
 * LICENSE-code file in the root directory of this repository.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef ADVOPTSDIALOG_HH
#define ADVOPTSDIALOG_HH

#include "horizonwizard.hh"
#include <QDialog>

class AdvOptsDialog : public QDialog {
public:
    AdvOptsDialog(HorizonWizard *wizard, QWidget *parent = nullptr);
};

#endif  /* !ADVOPTSDIALOG_HH */
