/*
 * pkgsimple.cc - Implementation of the UI.Packages.SimpleSel page
 * horizon-qt5, the Qt 5 user interface for
 * Project Horizon
 *
 * Copyright (c) 2019-2022 Adélie Linux and contributors.  All rights reserved.
 * This code is licensed under the AGPL 3.0 license, as noted in the
 * LICENSE-code file in the root directory of this repository.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "pkgsimple.hh"
#include "pkgdefaults.hh"

#include <QButtonGroup>
#include <QGridLayout>
#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>

PkgSimplePage::PkgSimplePage(QWidget *parent) : HorizonWizardPage(parent) {
    setTitle(tr("Software Selection"));
    loadWatermark("software");

    QLabel *descLabel = new QLabel(tr(
            "Select the software you want to install on this computer.\n\n"
            "You can install and uninstall more software at any time using the Package Manager.  For more information, see the User Handbook in Online Help."));
    descLabel->setWordWrap(true);

    QRadioButton *mobileButton, *compactButton, *textButton, *customButton;
    QLabel *standardLabel, *mobileLabel, *compactLabel, *textLabel,
            *customLabel;
    QTabWidget *tabWidget;

    standardButton = new QRadioButton(tr("&Standard"));
    standardButton->setIcon(QIcon::fromTheme("preferences-desktop-theme"));
    standardButton->setIconSize(QSize(32, 32));
    standardLabel = new QLabel(
            tr("Desktop environment and full software suite."));
    standardLabel->setBuddy(standardButton);
    standardLabel->setIndent(40);
    standardLabel->setWordWrap(true);
    standardButton->setWhatsThis(standardLabel->text());

    mobileButton = new QRadioButton(tr("&Mobile"));
    mobileButton->setIcon(QIcon::fromTheme("battery"));
    mobileButton->setIconSize(QSize(32, 32));
    mobileLabel = new QLabel(
            tr("Standard + extra software for portable computers."));
    mobileLabel->setBuddy(mobileButton);
    mobileLabel->setIndent(40);
    mobileLabel->setWordWrap(true);
    mobileButton->setWhatsThis(mobileLabel->text());

    compactButton = new QRadioButton(tr("&Compact"));
    compactButton->setIcon(QIcon::fromTheme("preferences-ubuntu-panel"));
    compactButton->setIconSize(QSize(32, 32));
    compactLabel = new QLabel(tr("Desktop environment and lightweight utilities only."));
    compactLabel->setBuddy(compactButton);
    compactLabel->setIndent(40);
    compactLabel->setWordWrap(true);
    compactButton->setWhatsThis(compactLabel->text());

    textButton = new QRadioButton(tr("&Text-Only"));
    textButton->setIcon(QIcon::fromTheme("utilities-terminal"));
    textButton->setIconSize(QSize(32, 32));
    textLabel = new QLabel(
            tr("Includes only text-mode support.  Select for servers, or computers with very limited resources."));
    textLabel->setBuddy(textButton);
    textLabel->setIndent(40);
    textLabel->setWordWrap(true);
    textButton->setWhatsThis(textLabel->text());

    customButton = new QRadioButton(tr("C&ustom"));
    customButton->setIcon(QIcon::fromTheme("preferences-activities"));
    customButton->setIconSize(QSize(32, 32));
    customLabel = new QLabel(tr("Customise the packages installed on your computer."));
    customLabel->setBuddy(customButton);
    customLabel->setIndent(40);
    customLabel->setWordWrap(true);
    customButton->setWhatsThis(customLabel->text());

    deskPlasmaButton = new QRadioButton(tr("KDE Plasma"));
    deskLXQtButton = new QRadioButton(tr("LXQt"));
    deskMATEButton = new QRadioButton(tr("MATE"));
    deskXFCEButton = new QRadioButton(tr("XFCE"));

    auto *desktopGroup = new QButtonGroup(this);
    desktopGroup->addButton(deskPlasmaButton, HorizonWizard::Plasma);
    desktopGroup->addButton(deskLXQtButton, HorizonWizard::LXQt);
    desktopGroup->addButton(deskMATEButton, HorizonWizard::MATE);
    desktopGroup->addButton(deskXFCEButton, HorizonWizard::XFCE);
    connect(desktopGroup, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonClicked),
            [this, desktopGroup](QAbstractButton *choice) {
        horizonWizard()->desktopType = static_cast<HorizonWizard::DesktopType>(desktopGroup->id(choice));
    });

    auto *desktopLayout = new QHBoxLayout;
    for(auto button: {deskPlasmaButton, deskLXQtButton, deskMATEButton, deskXFCEButton}) {
        desktopLayout->addWidget(button);
    }
    auto *gfxButtonLayout = new QVBoxLayout;
    gfxButtonLayout->addLayout(desktopLayout);
    gfxButtonLayout->addStretch();
    gfxButtonLayout->addWidget(standardButton);
    gfxButtonLayout->addWidget(standardLabel);
    gfxButtonLayout->addWidget(mobileButton);
    gfxButtonLayout->addWidget(mobileLabel);
    gfxButtonLayout->addWidget(compactButton);
    gfxButtonLayout->addWidget(compactLabel);
    auto *gfxTab = new QWidget;
    gfxTab->setLayout(gfxButtonLayout);

    auto *textButtonLayout = new QVBoxLayout;
    textButtonLayout->addWidget(textButton);
    textButtonLayout->addWidget(textLabel);
    auto *textTab = new QWidget;
    textTab->setLayout(textButtonLayout);

    auto *customButtonLayout = new QVBoxLayout;
    customButtonLayout->addWidget(customButton);
    customButtonLayout->addWidget(customLabel);
    auto *customTab = new QWidget;
    customTab->setLayout(customButtonLayout);

    auto *group = new QButtonGroup(this);
    group->addButton(standardButton, HorizonWizard::Standard);
    group->addButton(mobileButton, HorizonWizard::Mobile);
    group->addButton(compactButton, HorizonWizard::Compact);
    group->addButton(textButton, HorizonWizard::TextOnly);
    group->addButton(customButton, HorizonWizard::Custom);
    connect(group, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonClicked),
            [this, group](QAbstractButton *choice) {
        horizonWizard()->pkgtype = static_cast<HorizonWizard::PackageType>(group->id(choice));
    });

    tabWidget = new QTabWidget(this);
    tabWidget->addTab(gfxTab, tr("Desktop"));
    tabWidget->addTab(textTab, tr("Text/Server"));
    tabWidget->addTab(customTab, tr("Custom"));

    auto *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(descLabel);
    mainLayout->addStretch();
    mainLayout->addWidget(tabWidget);
    setLayout(mainLayout);
}

void PkgSimplePage::initializePage() {
    standardButton->click();
#ifdef HAS_INSTALL_ENV
    auto curr_desktop = qEnvironmentVariable("XDG_CURRENT_DESKTOP").toStdString();
    if(curr_desktop == "LXQt") {
        deskLXQtButton->click();
    } else if(curr_desktop == "MATE") {
        deskMATEButton->click();
    } else if(curr_desktop == "XFCE") {
        deskXFCEButton->click();
    } else
#endif  /* HAS_INSTALL_ENV */
        deskPlasmaButton->click();
}

int PkgSimplePage::nextId() const {
    if(horizonWizard()->pkgtype == HorizonWizard::Custom)
        return HorizonWizard::Page_PkgCustom;

    return HorizonWizard::Page_Boot;
}

bool PkgSimplePage::validatePage() {
    /* This code handles the following scenario:
     *
     * - 'Custom' is selected
     * - A non-default option is selected on the Defaults page (i.e., mdevd)
     * - Back is chosen
     * - A non-custom package selection choice is selected
     *
     * We need to completely 'reset' the state of the Defaults, including
     * the selections on the Defaults page, whenever Next is chosen.
     */
    if(horizonWizard()->pkgtype == HorizonWizard::Custom) {
        horizonWizard()->removePage(HorizonWizard::Page_PkgCustomDefault);
        horizonWizard()->setPage(HorizonWizard::Page_PkgCustomDefault, new PkgDefaultsPage);
    } else {
        horizonWizard()->sbininit = HorizonWizard::S6;
        horizonWizard()->binsh = HorizonWizard::Dash;
        horizonWizard()->eudev = true;
    }

    return true;
}
