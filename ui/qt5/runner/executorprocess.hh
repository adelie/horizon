/*
 * executorprocess.hh - Definition of the QProcess class for the executor
 * horizon-run-qt5, the Qt 5 executor user interface for
 * Project Horizon
 *
 * Copyright (c) 2023 Adélie Linux and contributors.  All rights reserved.
 * This code is licensed under the AGPL 3.0 license, as noted in the
 * LICENSE-code file in the root directory of this repository.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef EXECUTORPROCESS_HH
#define EXECUTORPROCESS_HH

#include <QProcess>

class ExecutorProcess : public QProcess {
public:
    ExecutorProcess(QObject *parent = nullptr) : QProcess(parent) {};
protected:
    void setupChildProcess() override;
};

#endif  /* !ERRORPAGE_HH */
