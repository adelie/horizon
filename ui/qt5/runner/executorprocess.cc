/*
 * executorprocess.cc - Implementation of the QProcess class for the executor
 * horizon-run-qt5, the Qt 5 executor user interface for
 * Project Horizon
 *
 * Copyright (c) 2023 Adélie Linux and contributors.  All rights reserved.
 * This code is licensed under the AGPL 3.0 license, as noted in the
 * LICENSE-code file in the root directory of this repository.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "executorprocess.hh"

#include <unistd.h>

void ExecutorProcess::setupChildProcess() {
    ::setuid(::geteuid());
    ::setgid(::getegid());
}
